import { describe } from 'riteway'
import Topology from '../index.js'

import { Model } from 'real-value-lang'
const debug = require('debug')('test')

let n = x => { // This is a node visitor function which is called as the expression AST is traversed.
  console.log(`Node ${x.name} at ${x.x}:${x.y}`)
}
let l = (n1, n2) => { // This is a link visitor function which is called as the expression AST is traversed.
  console.log(`${n1.name}=>${n2.name}`)
}

describe('Render Topology', async (assert) => {
  let amodel = Model()
  let stream = amodel.from('1,,,3', { name: 'csv1' })
  stream.log()
  stream.log()

  let topo = Topology({ node: n, link: l })
  topo.traverseModel(amodel)

  assert({
    given: 'a model and a from(csv)',
    should: 'the graph should be',
    actual: `${topo.render()}`,
    expected: '\tcsv1\tlog\n\t\tlog'
  })
})

describe('Render Topology', async (assert) => {
  let amodel = Model()
  amodel.from('1,,,3', { name: 'csv1' }).filter().tap()

  let topo = Topology({ node: n, link: l })
  topo.traverseModel(amodel)

  assert({
    given: 'a model and a from(csv)',
    should: 'the graph should be',
    actual: `${topo.render()}`,
    expected: '\tcsv1\tfilter\ttap'
  })

  let b = topo.bounds()

  assert({
    given: 'a model and a from(csv)',
    should: 'bounds be correct',
    actual: `${b[0]} ${b[1]}`,
    expected: '2 0'
  })
})

describe('Render Topology', async (assert) => {
  let amodel = Model()
  let s1 = amodel.from('1,2', { name: 's1' })
  let s2 = amodel.from('3,4', { name: 's2' })

  s1.merge(s2, { name: 'merge' })

  let topo = Topology({ node: n, link: l })
  topo.traverseModel(amodel)

  assert({
    given: 'a merge',
    should: 'the graph should be',
    actual: `${topo.render()}`,
    expected: '\ts1\tmerge1\tmerge\n\ts2\tmerge2'
  })

  let b = topo.bounds()

  assert({
    given: 'a model and a from(csv)',
    should: 'bounds be correct',
    actual: `${b[0]} ${b[1]}`,
    expected: '2 1'
  })
})

describe('Render Join', async (assert) => {
  let amodel = Model()
  let s1 = amodel.from([{ 'key': 1, 'b': 1 }, { 'key': 2, 'value': 2 }])
  let s2 = amodel.from([{ 'key': 1, 'c': 1 }, { 'key': 3, 'value': 3 }])
  s1.join(s2, { type: 'outer' })

  let topo = Topology({ node: n, link: l })
  topo.traverseModel(amodel)

  assert({
    given: 'a join',
    should: 'the graph should be',
    actual: `${topo.render()}`,
    expected: '\tfromArray\tjoin1\tfromGen\n\tfromArray\tjoin1'
  })

  let b = topo.bounds()

  assert({
    given: 'stream1 stream2 and a join',
    should: 'bounds be correct',
    actual: `${b[0]} ${b[1]}`,
    expected: '2 1'
  })
})
