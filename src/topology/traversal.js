/*
 * Traverses an entity with sources and sinks to build a topology
 */
const TopologyState = require('./topology.js')

module.exports = function (options = {}) {
    let thetopology = TopologyState()

    let { node: nodeHandler = null, link: linkHandler = null } = options

    function topology () {
        return thetopology
    }

    function traverseModel (model) {
        let next = { x: 0, y: 0 }
        for (let i = 0; i < model.sources().length; i++) {
            next.x = 0
            let node = model.sources()[i]
            traverseNode(node, next)
            next.y = next.y + 1
        }
    }

    function traverseNode (node, current) {
        let next = current

        let tnode = thetopology.getNode(node)
        if (!tnode) {
            tnode = thetopology.createNode(node, current)
            if (nodeHandler) {
                nodeHandler(tnode)
            }
            next.x = next.x + 1
        }

        let nextx = next.x
        for (let i = 0; i < node.sink().length; i++) {
            next.x = nextx
            next.y = next.y + i
            let sinkNode = node.sink()[i]
            let tNode2 = traverseNode(sinkNode, next)
            if (linkHandler) {
                linkHandler(tnode, tNode2)
            }
        }
        return tnode
    }

    function render () {
        return thetopology.render()
    }

    function bounds () {
        return thetopology.getBounds()
    }

    return {
        traverseModel,
        topology,
        render,
        bounds
    }
}
