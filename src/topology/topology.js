/*
 * Keeping track of layout of nodes in a 2d space
 */

const debug = require('debug')('topology')

module.exports = function () {
    let streamNodeMap = {}
    let streamNodeList = []

    function nodelist () {
        return streamNodeList
    }

    function getNode (node) {
        return streamNodeMap[node.id()]
    }

    function createNode (node, current) {
        function nameOf () {
            return this.name
        }

        function position () {
            return `${this.x},${this.y}`
        }

        debug(`${node.name} ${current.x} ${current.y}`)
            let n = {
                name: node.name,
                type: node.type,
                x: current.x,
                y: current.y,
                position,
                nameOf,
                stream: node
            }
            streamNodeMap[node.id()] = n
            streamNodeList.push(n)
            return n
    }

    function render () {
        let b = ''
        let row = 0
        let col = 0

        let plot = (j) => {
            let done = false
            while (!done) {
                if (streamNodeList[j].y === row && streamNodeList[j].x === col) {
                    b = b + '\t' + streamNodeList[j].name
                    col++
                    done = true
                } else if (streamNodeList[j].y > row) {
                    b = b + '\n'
                    row++
                    col = 0
                } else if (streamNodeList[j].x !== col) {
                    b = b + '\t'
                    col++
                }
            }
        }
        for (let j = 0; j < streamNodeList.length; j++) {
            plot(j)
        }
        return b
    }

    function getBounds () {
        let mX = 0; let mY = 0
        streamNodeList.forEach(n => {
            mX = mX > n.x ? mX : n.x
            mY = mY > n.y ? mY : n.y
        })
        return [mX, mY]
    }

    return {
        getNode,
        createNode,
        render,
        nodelist,
        getBounds
    }
}
