# real-value-topology Library     

This library is used to derive the topology of a real-value-lang expresssion and supports rendering visualizations of real-value-lang expressions.

## About

See the test/lib.test.js for examples of how to use.

## Install

```
npm install real-value-topology
yarn install real-value-topology
```

## How to use

see the unit test